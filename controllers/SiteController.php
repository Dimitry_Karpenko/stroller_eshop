<?php
// require_once(ROOT.'/models/ProductModel.php');
// require_once(ROOT.'/models/CategoryModel.php'); - теперь автолоудер подключает эти файлы автоматом

/**
 * 
 */
class SiteController
{
  
  public function actionIndex()
  {
    $commonData = CommonModel::getCommondata();    
    $categoryList = CategoryModel::getCategoryList();
    $recommendedCategories = CategoryModel::getRecommendedCategory();
    $title = 'Купить детскую коляску недорого в интернет магазине | КОЛЯСКА.УКР';
    $description = 'Купить прогулочную коляску - лучшую для путешествий. Купить коляску трость. Коляски для новорожденных. Коляски для двойни. Зимние и летние коляски с большими колесами.';

    if (CartModel::getProducts()) {
      $productsInCart = CartModel::getProducts();
      $productIds = array_keys($productsInCart);
      $products = ProductModel::getProductsByIds($productIds);
      $totalPrice = CartModel::getTotalPrice($products);
      $totalQuantity = CartModel::countItems();
    }  
    else{
      $totalPrice = 0;
    }    

    $saleProducts = ProductModel::getSaleProducts();
    $newProducts = ProductModel::getNewProducts();
    require_once(ROOT.'/views/site/index.php');
    return true;
  }
}